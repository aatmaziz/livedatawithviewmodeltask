package com.aait.testlivedataandviewmodel.Network;

import com.aait.testlivedataandviewmodel.Models.MyLastModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostApi {

    @GET("posts")
    Call<List<MyLastModel>> getPost();

}
