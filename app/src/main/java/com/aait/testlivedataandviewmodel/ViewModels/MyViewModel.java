package com.aait.testlivedataandviewmodel.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.aait.testlivedataandviewmodel.Models.MyLastModel;
import com.aait.testlivedataandviewmodel.MyResource.Resource;
import com.aait.testlivedataandviewmodel.repository.MyProjectRepository;

import java.util.List;

public class MyViewModel extends AndroidViewModel {

    MyProjectRepository myProjectRepository ;

    MutableLiveData<Resource> status;

    MyViewModel(Application application){
        super(application);
        myProjectRepository = new MyProjectRepository(application.getApplicationContext());
        myProjectRepository.getPost();
        status=myProjectRepository.getState();



    }
    public  LiveData<List<MyLastModel>> getPost(){

        return myProjectRepository.getPost();
   }
   public MutableLiveData<Resource> getstatus() {
        return status;
    }

}
