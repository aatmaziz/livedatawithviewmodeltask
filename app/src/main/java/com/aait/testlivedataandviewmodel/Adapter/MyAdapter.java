package com.aait.testlivedataandviewmodel.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.aait.testlivedataandviewmodel.Models.MyLastModel;
import com.aait.testlivedataandviewmodel.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    Context context ;
    List<MyLastModel> myServiceModels ;

    public MyAdapter(Context context, List<MyLastModel> myServiceModels) {
        this.context = context;
        this.myServiceModels = myServiceModels;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.txt.setText(myServiceModels.get(i).getTitle());
    }

    @Override
    public int getItemCount() {
        return myServiceModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txview);
        }
    }
}
