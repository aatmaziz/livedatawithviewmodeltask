package com.aait.testlivedataandviewmodel.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.widget.Toast;

import com.aait.testlivedataandviewmodel.Models.MyLastModel;
import com.aait.testlivedataandviewmodel.MyResource.Resource;
import com.aait.testlivedataandviewmodel.Network.PostApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class MyProjectRepository extends BaseRepository {

    private final Context context;
    private  MutableLiveData<Resource>  state;
    private PostApi postApi;

    public MutableLiveData<Resource> getState() {
        return state;
    }

    public MyProjectRepository(Context context) {
        this.context = context;
        state=new MutableLiveData<>();
        postApi = getRetrofitHelper().getService(PostApi.class);
    }


    public  LiveData<List<MyLastModel>> getPost() {

        postApi.getPost().enqueue(new Callback<List<MyLastModel>>() {
            @Override
            public void onResponse(Call<List<MyLastModel>> call, Response<List<MyLastModel>> response) {
                if (response.isSuccessful()){
                    state.postValue(Resource.success(response.body()));

                }else {
                    state.postValue(Resource.error(response.code()+"",null));
                }

            }

            @Override
            public void onFailure(Call<List<MyLastModel>> call, Throwable t) {
                state.postValue(Resource.error(t.getMessage(),null));

            }
        });

        return new LiveData<List<MyLastModel>>() {
        };
    }
}
