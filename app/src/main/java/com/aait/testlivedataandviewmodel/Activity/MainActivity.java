package com.aait.testlivedataandviewmodel.Activity;

import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.aait.testlivedataandviewmodel.Adapter.MyAdapter;
import com.aait.testlivedataandviewmodel.Models.MyLastModel;
import com.aait.testlivedataandviewmodel.MyResource.Resource;
import com.aait.testlivedataandviewmodel.R;
import com.aait.testlivedataandviewmodel.ViewModels.MyViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static com.aait.testlivedataandviewmodel.MyResource.Resource.Status.ERROR;
import static com.aait.testlivedataandviewmodel.MyResource.Resource.Status.LOADING;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView ;
    MyAdapter homeAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("wait");
        dialog.setCancelable(false);

        MyViewModel myViewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        myViewModel.getstatus().observe(this, new Observer<Resource>() {
            @Override
            public void onChanged(@Nullable Resource resource) {

                Log.e("states",new Gson().toJson(resource)+".");
                if (resource.status==LOADING){
                    dialog.show();
                }
                else {
                    dialog.hide();
                    if (resource.status==ERROR){

                    }
                    else {
                        ArrayList<MyLastModel> data = (ArrayList<MyLastModel>) resource.data;
                        Log.e("resp",new Gson().toJson(data)+".");
                        homeAdapter=new MyAdapter(getApplicationContext(),data);
                        recyclerView.setAdapter(homeAdapter);
                    }
                }

            }
        });

    }
}
